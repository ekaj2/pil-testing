from PIL import Image
import numpy as np

BW_IMAGE_FILENAME = "black_and_white.png"

# 'white' color...how sensitive should we be to white?
color_white = (200, 200, 200)

# baseline colors to replace with
color_black = (  0,   0,   0)
color_red   = (255,   0,   0)

# make a numpy array out of the RGB components of the image
with Image.open(BW_IMAGE_FILENAME) as img:
    data = np.array(img.convert('RGB'))

# =============================================================================
# For better results with lower resolution images, we need to do some aliasing
# here by applying a gradient across the values instead of an explicit boundary
# between black and white (color_white). Linear should be a large improvement
# and there would not be much added benefit to do anything fancier. Also, at a
# high enough resolution, this effect will not be noticed.
# =============================================================================

# replace 'blacks' with red and 'whites' with black
data[(data < color_white).all(axis = -1)] = color_red
data[(data >= color_white).all(axis = -1)] = color_black

# build a new PIL image
img = Image.fromarray(data, mode='RGB')

# show or save
img.show()
